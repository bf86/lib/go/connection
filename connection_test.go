package connection

import (
	"reflect"
	"testing"
)

type fields struct {
	Protocol string
	Host     string
	Port     string
}

func TestConnection_ConnectionString(t *testing.T) {
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{name: "Default", fields: fields{Protocol: "http", Host: "localhost", Port: "80"}, want: "http://localhost:80"},
		{name: "TCP", fields: fields{Protocol: "tcp", Host: "localhost", Port: "22"}, want: "tcp://localhost:22"},
		{name: "RabbitMQ", fields: fields{Protocol: "amqp", Host: "localhost", Port: "5672"}, want: "amqp://localhost:5672"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Connection{
				Protocol: tt.fields.Protocol,
				Host:     tt.fields.Host,
				Port:     tt.fields.Port,
			}
			if got := c.ConnectionString(); got != tt.want {
				t.Errorf("Connection.ConnectionString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetPrefix(t *testing.T) {
	tests := []struct {
		prefix string
		name   string
		fields fields
		want   string
	}{
		{name: "Default", fields: fields{}, want: ""},
		{name: "Custom", prefix: "CUSTOM", fields: fields{}, want: "CUSTOM_"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ConnectionEnvPrefix = tt.prefix
			if got := getPrefix(); got != tt.want {
				t.Errorf("Connection.GetPrefix() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewConnection(t *testing.T) {
	test := struct {
		name string
		want *Connection
	}{
		name: "Default",
		want: &Connection{Protocol: "https", Host: "host", Port: "3000"},
	}

	t.Run(test.name, func(t *testing.T) {
		if got := NewConnection(); !reflect.DeepEqual(got, test.want) {
			t.Errorf("NewConnection() = %v, want %v", got, test.want)
		}
	})
}

func Test_getProtocol(t *testing.T) {
	type args struct {
		fallback string
	}
	test := struct {
		name string
		args args
		want string
	}{name: "From fallback", args: args{fallback: "http"}, want: "http"}
	t.Run(test.name, func(t *testing.T) {
		if got := getProtocol(test.args.fallback); got != test.want {
			t.Errorf("getProtocol() = %v, want %v", got, test.want)
		}
	})
}

func Test_getHost(t *testing.T) {
	type args struct {
		fallback string
	}
	test := struct {
		name string
		args args
		want string
	}{name: "From fallback", args: args{fallback: "localhost"}, want: "localhost"}
	t.Run(test.name, func(t *testing.T) {
		if got := getHost(test.args.fallback); got != test.want {
			t.Errorf("getHost() = %v, want %v", got, test.want)
		}
	})
}

func Test_getPort(t *testing.T) {
	type args struct {
		fallback string
	}
	test := struct {
		name string
		args args
		want string
	}{name: "From fallback", args: args{fallback: "80"}, want: "80"}
	t.Run(test.name, func(t *testing.T) {
		if got := getPort(test.args.fallback); got != test.want {
			t.Errorf("getPort() = %v, want %v", got, test.want)
		}
	})
}

func TestNewConnectionFromEnv(t *testing.T) {
	getenv = func(string) string { return "env" }

	test := struct {
		name string
		want *Connection
	}{
		name: "Default",
		want: &Connection{Protocol: "env", Host: "env", Port: "env"},
	}

	t.Run(test.name, func(t *testing.T) {
		if got := NewConnection(); !reflect.DeepEqual(got, test.want) {
			t.Errorf("NewConnection() = %v, want %v", got, test.want)
		}
	})
}

func Test_getProtocolFromEnv(t *testing.T) {
	getenv = func(string) string { return "tcp" }

	type args struct {
		fallback string
	}
	test := struct {
		name string
		args args
		want string
	}{name: "From ENV", args: args{}, want: "tcp"}
	t.Run(test.name, func(t *testing.T) {
		if got := getProtocol(test.args.fallback); got != test.want {
			t.Errorf("getProtocol() = %v, want %v", got, test.want)
		}
	})
}

func Test_getHostFromEnv(t *testing.T) {
	getenv = func(string) string { return "localhost" }

	type args struct {
		fallback string
	}
	test := struct {
		name string
		args args
		want string
	}{name: "From ENV", args: args{}, want: "localhost"}
	t.Run(test.name, func(t *testing.T) {
		if got := getHost(test.args.fallback); got != test.want {
			t.Errorf("getHost() = %v, want %v", got, test.want)
		}
	})
}

func Test_getPortFromEnv(t *testing.T) {
	getenv = func(string) string { return "5000" }

	type args struct {
		fallback string
	}
	test := struct {
		name string
		args args
		want string
	}{name: "From ENV", args: args{}, want: "5000"}
	t.Run(test.name, func(t *testing.T) {
		if got := getPort(test.args.fallback); got != test.want {
			t.Errorf("getPort() = %v, want %v", got, test.want)
		}
	})
}
