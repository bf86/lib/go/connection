package connection

import (
	"fmt"
	"os"
)

var (
	getenv              = os.Getenv
	ConnectionEnvPrefix = ""
)

type Connection struct {
	Protocol string
	Host     string
	Port     string
}

type IConnection interface {
	ConnectionString() string
}

func NewConnection() *Connection {
	return &Connection{
		Protocol: getProtocol("https"),
		Host:     getHost("host"),
		Port:     getPort("3000"),
	}
}

func (c *Connection) ConnectionString() string {
	return fmt.Sprintf("%s://%s:%s", c.Protocol, c.Host, c.Port)
}

func getPrefix() string {
	if ConnectionEnvPrefix == "" {
		return ConnectionEnvPrefix
	}
	return ConnectionEnvPrefix + "_"
}

func getProtocol(fallback string) string {
	protocol := getenv(getPrefix() + "PROTOCOL")
	if protocol != "" {
		return protocol
	}
	return fallback
}

func getHost(fallback string) string {
	host := getenv(getPrefix() + "HOST")
	if host != "" {
		return host
	}
	return fallback
}

func getPort(fallback string) string {
	port := getenv(getPrefix() + "PORT")
	if port != "" {
		return port
	}
	return fallback
}
